import DMRoomMap from "matrix-react-sdk/src/utils/DMRoomMap";
import { MatrixClientPeg } from "matrix-react-sdk/src/MatrixClientPeg";
import { Room } from "matrix-js-sdk/src/models/room";

export default class RoomSearch {
    private prevSearchString: string = "";
    private memoSearchResult = {};
    private memoRooms: string[] = [];
    private static internalInstance = new RoomSearch();

    public static get instance(): RoomSearch {
        return RoomSearch.internalInstance;
    }

    public getRoomsBySearchString(value: string = "") {
        const searchString = value.trim();
        if (searchString === this.prevSearchString) {
            return this.memoSearchResult;
        }

        let rooms;
        if (this.prevSearchString && searchString.includes(this.prevSearchString)) {
            rooms = this.memoRooms;
        } else {
            const cli = MatrixClientPeg.get();
            if (!cli) {
                return null;
            }
            rooms = cli.getRooms();
        }

        const dmRoomMap = DMRoomMap.shared();
        const chats = [];
        const groups = [];
        const regexp = this.buildRegexp(searchString);

        for (const room of rooms) {
            const arr = this.getMembersNamesForRoom(room);
            arr.push(room.name);
            if (!arr.filter((str) => str.search(regexp) !== -1).length) {
                continue;
            }
            dmRoomMap.getUserIdForRoomId(room.roomId) ? chats.push(room) : groups.push(room);
        }
        const res = { chats, groups };

        this.memoSearchResult = res;
        this.memoRooms = chats.concat(groups);
        this.prevSearchString = searchString;
        return res;
    }

    private getMembersNamesForRoom(room: Room): string[] {
        const members = room.getMembersWithMembership("join");
        const res = [];
        for (const member of members) {
            if (member.userId === room.myUserId) {
                continue;
            }
            res.push(member.displayName || member.name || member.userId);
        }
        return res;
    }

    private buildRegexp(str): RegExp {
        const escapedStr = str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        return new RegExp(`(^|\\s|_|-|#)${escapedStr}`, 'i');
    }
}
