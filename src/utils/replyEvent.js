export const getReplyContentBody = (content) => {
    return content.formatted_body ? content.formatted_body.split('</mx-reply>')[1] : undefined;
};

export const isReplyEvent = (content) => {
    return content['m.relates_to'] && content['m.relates_to']['m.in_reply_to'];
};
