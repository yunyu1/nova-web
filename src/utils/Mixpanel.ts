import dis from 'matrix-react-sdk/src/dispatcher/dispatcher';
import { MatrixClientPeg } from "matrix-react-sdk/src/MatrixClientPeg";
import SdkConfig from 'matrix-react-sdk/src/SdkConfig';
import Timer from 'matrix-react-sdk/src/utils/Timer';
import { substitute } from 'matrix-react-sdk/src/languageHandler';

const UNAVAILABLE_TIME_MS = 3 * 60 * 1000; // 3 mins

class Detector {
    static getBrowser(userAgent: string, vendor: string, opera: boolean): string {
        vendor = vendor || ''; // vendor is undefined for at least IE9
        if (opera || userAgent.includes(' OPR/')) {
            if (userAgent.includes('Mini')) {
                return 'Opera Mini';
            }
            return 'Opera';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (userAgent.includes('IEMobile') || userAgent.includes('WPDesktop')) {
            return 'Internet Explorer Mobile';
        } else if (userAgent.includes('SamsungBrowser/')) {
            // https://developer.samsung.com/internet/user-agent-string-format
            return 'Samsung Internet';
        } else if (userAgent.includes('Edge') || userAgent.includes('Edg/')) {
            return 'Microsoft Edge';
        } else if (userAgent.includes('FBIOS')) {
            return 'Facebook Mobile';
        } else if (userAgent.includes('Chrome')) {
            return 'Chrome';
        } else if (userAgent.includes('CriOS')) {
            return 'Chrome iOS';
        } else if (userAgent.includes('UCWEB') || userAgent.includes('UCBrowser')) {
            return 'UC Browser';
        } else if (userAgent.includes('FxiOS')) {
            return 'Firefox iOS';
        } else if (vendor.includes('Apple')) {
            if (userAgent.includes('Mobile')) {
                return 'Mobile Safari';
            }
            return 'Safari';
        } else if (userAgent.includes('Android')) {
            return 'Android Mobile';
        } else if (userAgent.includes('Konqueror')) {
            return 'Konqueror';
        } else if (userAgent.includes('Firefox')) {
            return 'Firefox';
        } else if (userAgent.includes('MSIE') || userAgent.includes('Trident/')) {
            return 'Internet Explorer';
        } else if (userAgent.includes('Gecko')) {
            return 'Mozilla';
        } else {
            return '';
        }
    }

    static  getBrowserVersion(userAgent, vendor, opera) {
        const browser = this.getBrowser(userAgent, vendor, opera);
        const versionRegexs = {
            'Internet Explorer Mobile': /rv:(\d+(\.\d+)?)/,
            'Microsoft Edge': /Edge?\/(\d+(\.\d+)?)/,
            'Chrome': /Chrome\/(\d+(\.\d+)?)/,
            'Chrome iOS': /CriOS\/(\d+(\.\d+)?)/,
            'UC Browser' : /(UCBrowser|UCWEB)\/(\d+(\.\d+)?)/,
            'Safari': /Version\/(\d+(\.\d+)?)/,
            'Mobile Safari': /Version\/(\d+(\.\d+)?)/,
            'Opera': /(Opera|OPR)\/(\d+(\.\d+)?)/,
            'Firefox': /Firefox\/(\d+(\.\d+)?)/,
            'Firefox iOS': /FxiOS\/(\d+(\.\d+)?)/,
            'Konqueror': /Konqueror:(\d+(\.\d+)?)/,
            'BlackBerry': /BlackBerry (\d+(\.\d+)?)/,
            'Android Mobile': /android\s(\d+(\.\d+)?)/,
            'Samsung Internet': /SamsungBrowser\/(\d+(\.\d+)?)/,
            'Internet Explorer': /(rv:|MSIE )(\d+(\.\d+)?)/,
            'Mozilla': /rv:(\d+(\.\d+)?)/
        };
        const regex = versionRegexs[browser];
        if (regex === undefined) {
            return null;
        }
        const matches = userAgent.match(regex);
        if (!matches) {
            return null;
        }
        return parseFloat(matches[matches.length - 2]);
    }

    static  getOS(userAgent: string): string {
        if (/Windows/i.test(userAgent)) {
            if (/Phone/.test(userAgent) || /WPDesktop/.test(userAgent)) {
                return 'Windows Phone';
            }
            return 'Windows';
        } else if (/(iPhone|iPad|iPod)/.test(userAgent)) {
            return 'iOS';
        } else if (/Android/.test(userAgent)) {
            return 'Android';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (/Mac/i.test(userAgent)) {
            return 'Mac OS X';
        } else if (/Linux/.test(userAgent)) {
            return 'Linux';
        } else if (/CrOS/.test(userAgent)) {
            return 'Chrome OS';
        } else {
            return '';
        }
    }

    static  getDevice(userAgent: string): string {
        if (/Windows Phone/i.test(userAgent) || /WPDesktop/.test(userAgent)) {
            return 'Windows Phone';
        } else if (/iPad/.test(userAgent)) {
            return 'iPad';
        } else if (/iPod/.test(userAgent)) {
            return 'iPod Touch';
        } else if (/iPhone/.test(userAgent)) {
            return 'iPhone';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (/Android/.test(userAgent)) {
            return 'Android';
        } else {
            return '';
        }
    }

    static  referringDomain(referrer: string): string {
        const split = referrer.split('/');
        if (split.length >= 3) {
            return split[2];
        }
        return '';
    }
}

class Mixpanel {
    private permanentProps: object | null = null;
    private baseUrl: string | null = null;
    private trackToken: string | null = null;

    private trackingEnabled: boolean = false;
    private clientStarted: boolean = false;
    private online: boolean = false;

    private dispatcherRef: any;
    private unavailableTimer: Timer | null = null;

    constructor() {
        this.onAction = this.onAction.bind(this);
        this.dispatcherRef = dis.register(this.onAction);
    }

    public async updateProfile() {
        if (!this.trackingEnabled) {
            return;
        }
        console.info(`Send request to update user profile`);
        try {
            const userAgent = navigator.userAgent;
            const isOpera = Boolean(window.opera);
            const appHash = SdkConfig.get().app_hash;
            await fetch(`${this.baseUrl}/api/engage`, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    Authorization: `Bearer ${this.trackToken}`,
                },
                body: JSON.stringify({
                    '$set': {
                        '$os': Detector.getOS(userAgent),
                        '$browser': Detector.getBrowser(userAgent, navigator.vendor, isOpera),
                        'todesktop_version': window.todesktop ? window.todesktop.version : null,
                        'app_hash': appHash || 'UNKNOWN',
                        '$name': localStorage.getItem('mx_user_id')
                    }
                }),
            });
        } catch (err) {
            console.warn("Updating profile threw exception:", err)
        }
    }

    public async track(event: string, properties: Object = {}) {
        if (!this.trackingEnabled) {
            return;
        }
        console.info(`Send request to track event: ${event}`);
        if (!this.permanentProps) {
            const userAgent = navigator.userAgent;
            const isOpera = Boolean(window.opera);
            const appHash = SdkConfig.get().app_hash;
            this.permanentProps = {
                '$os': Detector.getOS(userAgent),
                '$browser': Detector.getBrowser(userAgent, navigator.vendor, isOpera),
                '$browser_version': Detector.getBrowserVersion(userAgent, navigator.vendor, isOpera),
                '$referrer': document.referrer,
                '$referring_domain': Detector.referringDomain(document.referrer),
                '$device': Detector.getDevice(userAgent),
                '$device_id': localStorage.getItem('mx_device_id') || '',
                'todesktop_version': window.todesktop ? window.todesktop.version : null,
                'app_hash': appHash || 'UNKNOWN',
            }
        }
        properties = {
            '$current_url': window.location.href,
            '$screen_height': screen.height,
            '$screen_width': screen.width,
            'time': Date.now() / 1000, // epoch time in seconds,
            ...this.permanentProps,
            ...properties
        };
        // clean up props
        for (const key of Object.keys(properties)) {
            if (!properties[key]) {
                delete properties[key];
            }
        }
        try {
            await fetch(`${this.baseUrl}/api/track`, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    Authorization: `Bearer ${this.trackToken}`,
                },
                body: JSON.stringify({
                    event,
                    properties,
                }),
            });
        } catch (err) {
            console.warn("Tracking threw exception:", err)
        }
    }

    private async init() {
        if (this.clientStarted) {
            return;
        }
        this.clientStarted = true;
        const client = MatrixClientPeg.get();
        const username = client.getUserId().split(':')[0].slice(1);
        this.baseUrl = substitute(SdkConfig.get().bridges_manager_url, { username });

        this.trackToken = window.localStorage.getItem("nv_analytics_token");
        const shouldTrackLogin = !this.trackToken;

        if (!this.trackToken) {
            const openIdTokenData = await client.getOpenIdToken();
            this.trackToken = await this.requestIntegrationToken(openIdTokenData);
            window.localStorage.setItem("nv_analytics_token", this.trackToken);
        }

        this.trackingEnabled = await this.prepareTracking();

        if (!this.trackingEnabled) {
            console.warn('Mixpanel tracking disabled');
            return;
        }
        console.info('Mixpanel tracking enabled');
        if (shouldTrackLogin) {
            await this.track("Nova-web login", { method: "direct" });
        }
        this.updateProfile();
        this.startPresenceSync();
    }

    private stop() {
        this.togglePresence(false);
        if (this.dispatcherRef) {
            dis.unregister(this.dispatcherRef);
            this.dispatcherRef = null;
        }
        if (this.unavailableTimer) {
            this.unavailableTimer.abort();
            this.unavailableTimer = null;
        }
        this.baseUrl = null;
        this.trackToken = null;
        this.trackingEnabled = false;
        this.clientStarted = false;
    }

    public onAction(payload) {
        switch (payload.action) {
            case 'client_started':
                this.init();
                break;
            case 'client_stopped':
                this.stop();
                break;
            case 'user_activity':
                if (this.trackingEnabled && this.unavailableTimer) {
                    this.togglePresence(true);
                    this.unavailableTimer.restart();
                }
        }
    }

    private async startPresenceSync() {
        this.unavailableTimer = new Timer(UNAVAILABLE_TIME_MS);
        while (this.unavailableTimer) {
            try {
                await this.unavailableTimer.finished();
                this.togglePresence(false);
            } catch (e) {}
        }
    }

    public async prepareTracking(): Promise<boolean> {
        try {
            const res = await fetch(`${this.baseUrl}/api/track`, {
                method: 'GET',
                mode: 'cors',
                headers: {
                    Authorization: `Bearer ${this.trackToken}`,
                    'Content-Type': 'application/json',
                }
            });
            const data = await res.json();
            return  data.enabled;
        } catch (err) {
            console.error("Error checking if Mixpanel tracking is enabled:", err)
            return false;
        }
    }

    private async requestIntegrationToken(tokenData): Promise<string> {
        try {
            const res = await fetch(`${this.baseUrl}/_matrix/integrations/v1/account/register`, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(tokenData),
            });
            const data = await res.json()
            return  data.token;
        } catch (err) {
            console.error("Error registering integration token:", err)
            return null;
        }
    }

    private togglePresence(isOnline: boolean) {
        if (this.online === isOnline) {
            return;
        }
        if (isOnline) {
            this.track('$session_start');
        } else {
            const lastActiveTime = Date.now() - UNAVAILABLE_TIME_MS;
            this.track('$session_end', { 'time': lastActiveTime / 1000 });
        }
        this.online = isOnline;
    }
}

export default new Mixpanel();
